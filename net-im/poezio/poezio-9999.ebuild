# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_DEPEND="3"

EGIT_REPO_URI="http://git.poezio.eu/poezio/"

inherit python git-2

DESCRIPTION="Console XMPP client that looks like most famous IRC clients"
HOMEPAGE="http://poezio.eu/"

LICENSE="custom"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=">=dev-python/sleekxmpp-9999 >=dev-python/dnspython3-1.10.0"
RDEPEND="${DEPEND}"

src_install()
{
	emake DESTDIR="${D}" prefix="/usr/" install || die "make install failed"
}
