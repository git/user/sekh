# Copyright 1999-2013 Gentoo Foundation
# Copyright 2013 William Lallemand <wlallemand@irq6.net>
# Distributed under the terms of the GNU General Public License v2

EAPI=3

IUSE="cxx"

inherit eutils

PATCH_VER="1.6"
UCLIBC_VER="1.0"
HOMEPAGE="https://launchpad.net/gcc-linaro"
# Hardened gcc 4 stuff
PIE_VER="0.5.5"
SPECS_VER="0.2.0"
SPECS_GCC_VER="4.4.3"
# arch/libc configurations known to be stable with {PIE,SSP}-by-default
PIE_GLIBC_STABLE="x86 amd64 ppc ppc64 arm ia64"
PIE_UCLIBC_STABLE="x86 arm amd64 ppc ppc64"
SSP_STABLE="amd64 x86 ppc ppc64 arm"
# uclibc need tls and nptl support for SSP support
# uclibc need to be >= 0.9.33
SSP_UCLIBC_STABLE="x86 amd64 ppc ppc64 arm"
#end Hardened stuff

inherit toolchain

GCC_RELEASE_VER=${PV}
GCC_VER="${PV/_p*/}"
DATE_VERSION="${PV/*_p}"
DATE_YEAR="${DATE_VERSION:0:4}"
DATE_MONTH="${DATE_VERSION:4:6}"
LINARO_VER="${GCC_VER}-${DATE_YEAR}.${DATE_MONTH}"
SRC_URI="https://launchpad.net/gcc-linaro/${GCC_VER}/${LINARO_VER}/+download/gcc-linaro-${LINARO_VER}.tar.bz2"


DESCRIPTION="The GNU Compiler Collection with Linaro patches"
LICENSE="GPL-3+ LGPL-3+ || ( GPL-3+ libgcc libstdc++ gcc-runtime-library-exception-3.1 ) FDL-1.3+"

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86
~amd64-fbsd ~x86-fbsd"

RDEPEND=""
DEPEND="${RDEPEND}
	elibc_glibc? ( >=sys-libs/glibc-2.8 )
	>=${CATEGORY}/binutils-2.18"

if [[ ${CATEGORY} != cross-* ]] ; then
	PDEPEND="${PDEPEND} elibc_glibc? ( >=sys-libs/glibc-2.8 )"
fi


src_unpack() {
	unpack ${A}
	export S="${WORKDIR}/gcc-linaro-${LINARO_VER}"
	export BRANDING_GCC_PKGVERSION="Linaro ${LINARO_VER}"
}

pkg_setup() {
	toolchain_pkg_setup
}
